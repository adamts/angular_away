import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>(); 


 post:Post;
  isEdit : boolean = false;
  editButtonText = 'Edit';
  constructor() { }
  sendDelete(){
     this.deleteEvent.emit(this.post);
    }
  
  toggleEdit(){
    //update parent about the change
   this.isEdit = !this.isEdit; 
    this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 
    if(!this.isEdit){
    this.editEvent.emit(this.post);
  }   

  }
  ngOnInit() {
  }

}