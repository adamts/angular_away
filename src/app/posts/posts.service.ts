import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {
  //private _url = "http://jsonplaceholder.typicode.com/users";//we remove this line because we want the data from firebase
  
postsObservable;

  getPosts(){

    this.postsObservable = this.af.database.list('/posts');//.map(
      //users =>{
        //users.map(
          //user => {
            //user.posTities = [];
            //for(var p in user.posts){
              //user.posTitles.push(
                //this.af.database.object('/posts/' + p)
              //)
            //}

            
          //}

        //);
        //return users;
      //}
    //)
  return this.postsObservable;
 // return this._http.get(this._url).map(res => res.json()).delay(500)     // res is just the name, like result  // delay is for the spinner // we don't want to read from url only from firebase
	
	}

  addPost (post){
 this.postsObservable.push(post);   
 }

 updatePost(post){
  let postKey = post.$key;
  let postData = {name:post.title};
  this.af.database.object('/post/' + postKey).update(postData);

 }
 deletePost(post){
 let postKey = post.$key;
  this.af.database.object('/posts/' + postKey).remove();
}



   constructor(private af:AngularFire) { } // לא קריטי להוסיף קו תחתון לפני השם , זאת לא דרישה של סינטקס
}