import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';

import { UsersService } from './users/users.service';
import { PostsService } from './posts/posts.service';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AngularFireModule } from 'angularfire2';
import { PostComponent } from './post/post.component';

import { PostFormComponent } from './post-form/post-form.component'; // for firebase

export const firebaseConfig = {
    apiKey: "AIzaSyCnNhVCNpOqzO3OsCL7RPZSC_QEm0zoSSw",
    authDomain: "adam-a5f43.firebaseapp.com",
    databaseURL: "https://adam-a5f43.firebaseio.com",
    storageBucket: "adam-a5f43.appspot.com",
    messagingSenderId: "780221808616"
}




const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },

  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent } //thit line need to be last 
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostComponent,
    PostComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig) // for firebase

  ],
  providers: [UsersService,PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }